// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package api

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"go.nlx.io/demo/basisregister-fictieve-personen/pkg/database/queries"
)

type Persoon struct {
	URL                    string     `json:"url"`
	Identificatie          string     `json:"identificatie"`
	Burgerservicenummer    string     `json:"burgerservicenummer"`
	Emailadres             string     `json:"emailadres"`
	Telefoonnummer         string     `json:"telefoonnummer"`
	Aanschrijving          *Naam      `json:"aanschrijving"`
	Naam                   *Naam      `json:"naam"`
	Partner                *Persoon   `json:"partner"`
	Ouders                 []*Persoon `json:"ouders"`
	Kinderen               []*Persoon `json:"kinderen"`
	AanduidingNaamsgebruik string     `json:"aanduiding_naamsgebruik"`
	Verblijfadres          *Adres     `json:"verblijfadres"`
	Postadres              *Adres     `json:"postadres"`
	Geboorte               *Datum     `json:"geboorte"`
	Overlijden             *Datum     `json:"overlijden"`
}

type Naam struct {
	Voornamen                string `json:"voornamen"`
	Voorletters              string `json:"voorletters"`
	AdelijkeTitel            string `json:"adelijke_titel"`
	VoorvoegselGeslachtsnaam string `json:"voorvoegsel_geslachtsnaam"`
	Geslachtsnaam            string `json:"geslachtsnaam"`
}

type Adres struct {
	Straatnaam string `json:"straatnaam"`
	Huisnummer int    `json:"huisnummer"`
	Postcode   string `json:"postcode"`
	Woonplaats string `json:"woonplaats"`
}

type Datum struct {
	Datum string `json:"datum"`
	Land  string `json:"land"`
	Stad  string `json:"stad"`
}

func createRouter(s *API) *chi.Mux {
	r := chi.NewRouter()

	r.Get("/natuurlijke_personen", func(w http.ResponseWriter, req *http.Request) {
		dbPersons, err := s.db.Queries.ListPersonen(context.TODO())
		if err != nil {
			writeError(w, err)
			return
		}

		persons := make([]*Persoon, len(dbPersons))

		for i, p := range dbPersons {
			persons[i], err = convertToPersoon(p)
			if err != nil {
				writeError(w, err)
				return
			}
		}

		type resp struct {
			Resultaten []*Persoon `json:"resultaten"`
			Aantal     int        `json:"aantal"`
		}

		r, err := json.Marshal(resp{
			Aantal:     len(persons),
			Resultaten: persons,
		})
		if err != nil {
			writeError(w, err)
			return
		}

		w.Write(r)
	})

	r.Post("/natuurlijke_personen", func(w http.ResponseWriter, req *http.Request) {
		reqBody, err := ioutil.ReadAll(req.Body)
		if err != nil {
			writeError(w, err)
			return
		}

		p := &Persoon{}

		err = json.Unmarshal(reqBody, p)
		if err != nil {
			writeError(w, err)
			return
		}

		data, err := json.Marshal(p)
		if err != nil {
			writeError(w, err)
			return
		}

		parsedID, err := uuid.Parse(p.Identificatie)
		if err != nil {
			writeError(w, err)
			return
		}

		err = s.db.Queries.CreatePersoon(context.TODO(), &queries.CreatePersoonParams{
			Identificatie: parsedID,
			Data:          data,
		})
		if err != nil {
			writeError(w, err)
			return
		}
	})

	r.Get("/natuurlijke_personen/bsn/{bsn}", func(w http.ResponseWriter, req *http.Request) {
		bsn := chi.URLParam(req, "bsn")

		dbPerson, err := s.db.Queries.GetPersoonByBsn(context.TODO(), []byte(bsn))
		if err != nil {
			writeError(w, err)
			return
		}

		person, err := convertToPersoon(dbPerson)
		if err != nil {
			writeError(w, err)
			return
		}

		type resp struct {
			Persoon *Persoon `json:"persoon"`
		}

		r, err := json.Marshal(resp{
			Persoon: person,
		})
		if err != nil {
			writeError(w, err)
			return
		}

		w.Write(r)
	})

	r.Get("/natuurlijke_personen/{id}", func(w http.ResponseWriter, req *http.Request) {
		id := chi.URLParam(req, "id")

		parsedID, err := uuid.Parse(id)
		if err != nil {
			writeError(w, err)
			return
		}

		dbPerson, err := s.db.Queries.GetPersoon(context.TODO(), parsedID)
		if err != nil {
			writeError(w, err)
			return
		}

		person, err := convertToPersoon(dbPerson)
		if err != nil {
			writeError(w, err)
			return
		}

		type resp struct {
			Persoon *Persoon `json:"persoon"`
		}

		r, err := json.Marshal(resp{
			Persoon: person,
		})
		if err != nil {
			writeError(w, err)
			return
		}

		w.Write(r)
	})

	r.Put("/natuurlijke_personen/{id}", func(w http.ResponseWriter, req *http.Request) {
		id := chi.URLParam(req, "id")

		reqBody, err := ioutil.ReadAll(req.Body)
		if err != nil {
			writeError(w, err)
			return
		}

		p := &Persoon{}

		err = json.Unmarshal(reqBody, p)
		if err != nil {
			writeError(w, err)
			return
		}

		p.Identificatie = id

		data, err := json.Marshal(p)
		if err != nil {
			writeError(w, err)
			return
		}

		parsedID, err := uuid.Parse(id)
		if err != nil {
			writeError(w, err)
			return
		}

		err = s.db.Queries.UpdatePersoon(context.TODO(), &queries.UpdatePersoonParams{
			Identificatie: parsedID,
			Data:          data,
		})
		if err != nil {
			writeError(w, err)
			return
		}
	})

	r.Put("/natuurlijke_personen/{id}/uitschrijven", func(w http.ResponseWriter, req *http.Request) {
		id := chi.URLParam(req, "id")

		parsedID, err := uuid.Parse(id)
		if err != nil {
			writeError(w, err)
			return
		}

		err = s.db.Queries.DeregisterPersoon(context.TODO(), parsedID)
		if err != nil {
			writeError(w, err)
			return
		}
	})

	r.Put("/natuurlijke_personen/{id}/inschrijven", func(w http.ResponseWriter, req *http.Request) {
		id := chi.URLParam(req, "id")

		parsedID, err := uuid.Parse(id)
		if err != nil {
			writeError(w, err)
			return
		}

		reqBody, err := ioutil.ReadAll(req.Body)
		if err != nil {
			writeError(w, err)
			return
		}

		p := &Adres{}

		err = json.Unmarshal(reqBody, p)
		if err != nil {
			writeError(w, err)
			return
		}

		data, err := json.Marshal(p)
		if err != nil {
			writeError(w, err)
			return
		}

		err = s.db.Queries.RegisterPersoon(context.TODO(), &queries.RegisterPersoonParams{
			Identificatie: parsedID,
			Address:       data,
		})
		if err != nil {
			writeError(w, err)
			return
		}
	})

	r.Delete("/natuurlijke_personen/{id}", func(w http.ResponseWriter, req *http.Request) {
		id := chi.URLParam(req, "id")

		parsedID, err := uuid.Parse(id)
		if err != nil {
			writeError(w, err)
			return
		}

		err = s.db.Queries.DeletePersoon(context.TODO(), parsedID)
		if err != nil {
			writeError(w, err)
			return
		}
	})

	return r
}

func writeError(w http.ResponseWriter, err error) {
	http.Error(w, err.Error(), http.StatusInternalServerError)
}

func convertToPersoon(p *queries.NlxDemoBrpPersonen) (*Persoon, error) {
	person := &Persoon{}

	err := json.Unmarshal(p.Data, person)
	if err != nil {
		return nil, err
	}

	return person, nil
}
