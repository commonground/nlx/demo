BEGIN transaction;

CREATE SCHEMA nlx_demo_brp;

CREATE TABLE nlx_demo_brp.personen (
  identificatie UUID PRIMARY KEY,
  data JSON NOT NULL
);

COMMIT;
