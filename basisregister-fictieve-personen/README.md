# Basisregister Fictieve Personen

## Development
Start development server. Server will be started on port `8080`

```shell
make up
```

Stop development server

```shell
make down
```

See logs of development server

```shell
make logs
```

Rebuild development server

```shell
make build
```
