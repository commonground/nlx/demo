// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"go.nlx.io/demo/basisregister-fictieve-personen/pkg/api"
	"go.nlx.io/demo/basisregister-fictieve-personen/pkg/database"
	"go.nlx.io/demo/basisregister-fictieve-personen/pkg/seeder"
)

var serveOpts struct {
	ListenAddress string
	PostgresDSN   string
	Municipality  string
}

//nolint:gochecknoinits,gocyclo // this is the recommended way to use cobra
func init() {
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddress, "listen-address", "", "0.0.0.0:8080", "Address for the api to listen on.")
	serveCommand.Flags().StringVarP(&serveOpts.PostgresDSN, "postgres-dsn", "", "", "Postgres Connection URL")
	serveCommand.Flags().StringVarP(&serveOpts.Municipality, "municipality", "", "", "Municipality to seed data for")

	// Required flags
	if err := serveCommand.MarkFlagRequired("postgres-dsn"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("municipality"); err != nil {
		log.Fatal(err)
	}
}

var serveCommand = &cobra.Command{
	Use:   "serve",
	Short: "Start the api",
	Run: func(cmd *cobra.Command, args []string) {
		config := zap.NewDevelopmentConfig()
		config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
		config.Level = zap.NewAtomicLevelAt(zap.DebugLevel)

		logger, err := config.Build()
		if err != nil {
			log.Fatalf("failed to create new zap logger: %v", err)
		}

		logger.Info("Migrating DB")
		err = database.PostgresPerformMigrations(serveOpts.PostgresDSN)
		if err != nil {
			log.Fatalf("failed to migrate db: %v", err)
		}

		db, err := database.New(logger, serveOpts.PostgresDSN)
		if err != nil {
			log.Fatalf("failed to connect to the database: %v", err)
		}

		logger.Info("Seeding DB")
		err = seeder.Seed(db, serveOpts.Municipality)
		if err != nil {
			log.Fatalf("failed to seed database: %v", err)
		}

		logger.Info("starting api", zap.String("listen-address", serveOpts.ListenAddress))

		a, err := api.New(
			&api.NewAPIArgs{
				DB:            db,
				Logger:        logger,
				ListenAddress: serveOpts.ListenAddress,
			},
		)
		if err != nil {
			logger.Fatal("cannot setup api", zap.Error(err))
		}

		err = a.ListenAndServe()
		if err != nil {
			logger.Fatal("failed to listen and serve", zap.Error(err))
		}
	},
}
