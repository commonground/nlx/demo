// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
module.exports = {
  publicRuntimeConfig: {
    ORGANIZATION_NAME: process.env.ORGANIZATION_NAME,
    OUTWAY_PROXY_URL: process.env.OUTWAY_PROXY_URL,
  },
}
