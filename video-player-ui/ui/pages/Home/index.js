// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import Head from 'next/head'
import getConfig from 'next/config'
import { Container, Video } from './index.styles'

const { publicRuntimeConfig } = getConfig()

export default function Home() {
  const organizationName = publicRuntimeConfig.ORGANIZATION_NAME
  const outwayProxyURL = publicRuntimeConfig.OUTWAY_PROXY_URL

  return (
    <>
      <Head>
        <title>{organizationName} - NLX demo</title>
      </Head>

      <Container>
        <h1>Video streaming example using NLX</h1>
        <p>
          Example application to demonstrate that it is possible to stream video
          using NLX. The video below is being served by an API that is exposed
          on the NLX network.
        </p>

        <h2>The request flow</h2>
        <p>NextJS UI (video player) → Proxy → Outway → Inway → Service (API)</p>
        <p>
          <code>{outwayProxyURL}</code> is being used to serve the video.
        </p>
        <p>
          Make sure you have access to the service that is serving the video.
        </p>

        <h2>Big Buck Bunny</h2>

        <Video
          src={`${outwayProxyURL}/gtv-videos-bucket/sample/BigBuckBunny.mp4`}
          controls
        >
          <a
            href={`${outwayProxyURL}/gtv-videos-bucket/sample/BigBuckBunny.mp4`}
          >
            Download video
          </a>
        </Video>

        <p>
          Source <a href="https://peach.blender.org/about/">Blender.org</a>
        </p>
      </Container>
    </>
  )
}
