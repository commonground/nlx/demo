// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'

export const Container = styled.div`
  margin: 10vh auto 0 auto;
  width: 650px;
  max-width: 100%;
`

export const Video = styled.video`
  width: 100%;
`
