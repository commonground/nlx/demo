// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import { defaultTheme } from '@commonground/design-system'
import getConfig from 'next/config'

const { publicRuntimeConfig } = getConfig()

const tokens = {
  ...defaultTheme.tokens,

  colorBrand1: publicRuntimeConfig.ORGANIZATION_COLOR_PRIMARY,
}

const theme = {
  ...defaultTheme,
  tokens,
}

export default theme
