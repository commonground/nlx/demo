package main

type addPage struct {
	*basePage

	Municipalities []string
	Licenses       []string
	SuccessMessage string
	Form           *addPageForm
}

type addPageForm struct {
	ParkeerrechtenGrantHash string
	KentekenGrantHash       string
	Municipality            string
	GrantHash               string
	License                 string
	Year                    string
}
