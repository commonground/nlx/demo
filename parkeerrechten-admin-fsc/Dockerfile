# Copyright © VNG Realisatie 2023
# Licensed under the EUPL

FROM golang:1.20.5-alpine AS build

# Install build tools.
RUN apk add --update git gcc musl-dev

WORKDIR /api

# Cache dependencies
COPY go.mod go.mod
COPY go.sum go.sum
ENV GO111MODULE on
RUN go mod download

# Copy code that we use for building the api
COPY . .

ARG GIT_TAG_NAME=undefined
ARG GIT_COMMIT_HASH=undefined

RUN go build -o dist/bin/api ./

FROM alpine:3.18.2
COPY --from=build /api/dist/bin/api /usr/local/bin/api
COPY  ./static/ /static

# Add non-privileged user
RUN adduser -D -u 1001 appuser
USER appuser

EXPOSE 8081

CMD ["/usr/local/bin/api"]
