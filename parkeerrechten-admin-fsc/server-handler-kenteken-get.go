package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
)

type kentekenPartial struct {
	Handelsbenaming string
}

func (s *Server) handlerKentekenGet(w http.ResponseWriter, r *http.Request) {
	grantHash := r.URL.Query().Get("kentekenGrantHash")
	license := r.URL.Query().Get("license")

	kenteken, err := fetchKenteken(s.outwayAddress, license, grantHash)
	if err != nil {
		http.Error(w, fmt.Sprintf("failed to fetch kenteken: %s", err), http.StatusInternalServerError)

		return
	}

	p := kentekenPartial{
		Handelsbenaming: kenteken.Handelsbenaming,
	}

	t, err := template.ParseFS(
		tplFolder, "templates/kenteken.html")
	if err != nil {
		log.Printf("failed to parse kenteken template: %s", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	err = t.ExecuteTemplate(w, "kenteken.html", p)
	if err != nil {
		log.Printf("failed to render kenteken template: %s", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}

func fetchKenteken(outwayBaseURL, kenteken, grantHash string) (*APIResponseKenteken, error) {
	fmt.Printf("fetch kenteken\n")

	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/voertuig/%s", outwayBaseURL, kenteken), nil)
	if err != nil {
		return nil, fmt.Errorf("client: could not create request: %s", err)
	}

	req.Header.Add("Fsc-Grant-Hash", grantHash)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("client: error making http request: %s", err)
	}

	fmt.Printf("client: got response!\n")
	fmt.Printf("client: status code: %d\n", res.StatusCode)

	body, err := io.ReadAll(res.Body)
	if err != nil {
		fmt.Printf("client: could not read response body: %s\n", err)
		os.Exit(1)
	}
	fmt.Printf("client: response body: %s\n", body)

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("client: unexpected status code: %d, error: %s", res.StatusCode, body)
	}

	var apiResponse APIResponseKenteken
	err = json.Unmarshal(body, &apiResponse)
	if err != nil {
		return nil, fmt.Errorf("Error unmarshaling JSON: %s", err)
	}

	return &apiResponse, nil
}

type APIResponseKenteken struct {
	Handelsbenaming string `json:"handelsbenaming"`
}
