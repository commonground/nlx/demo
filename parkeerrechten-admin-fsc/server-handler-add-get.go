package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"
)

func (s *Server) handlerAddGet(w http.ResponseWriter, r *http.Request) {
	p := addPage{
		basePage: &basePage{
			SelfOrganizationName: s.selfOrganizationName,
			ActivePagePath:       PagePathAdd,
			PagePathHome:         PagePathHome,
			PagePathRetrieve:     PagePathRetrieve,
			PagePathAdd:          PagePathAdd,
		},
		Municipalities: s.municipalities,
		Licenses: []string{
			"RT774D",
			"KN958B",
			"81HZFB",
			"GJ713R",
			"50HSZS",
			"KS98DN",
		},
		Form: &addPageForm{
			ParkeerrechtenGrantHash: "",
			KentekenGrantHash:       "",
			Municipality:            "",
			GrantHash:               "",
			License:                 "",
			Year:                    fmt.Sprintf("%d", time.Now().Year()),
		},
	}

	t, err := template.ParseFS(
		tplFolder,
		"templates/base.html",
		"templates/add.html",
	)
	if err != nil {
		log.Printf("failed to parse template: %s", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	err = t.ExecuteTemplate(w, "base.html", p)
	if err != nil {
		log.Printf("failed to render template: %s", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}
