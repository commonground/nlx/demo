package main

import (
	"html/template"
	"log"
	"net/http"
)

func (s *Server) handlerHome(w http.ResponseWriter, r *http.Request) {
	p := homePage{
		basePage: &basePage{
			SelfOrganizationName: s.selfOrganizationName,
			ActivePagePath:       "/",
			PagePathHome:         PagePathHome,
			PagePathRetrieve:     PagePathRetrieve,
			PagePathAdd:          PagePathAdd,
		},
		Municipalities: s.municipalities,
	}

	t, err := template.ParseFS(
		tplFolder,
		"templates/base.html",
		"templates/home.html",
	)
	if err != nil {
		log.Printf("failed to parse template: %s", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	err = t.ExecuteTemplate(w, "base.html", p)
	if err != nil {
		log.Printf("failed to render template: %s", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}

type homePage struct {
	*basePage

	Municipalities []string
}
