package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
)

func (s *Server) handlerRetrieve(w http.ResponseWriter, r *http.Request) {
	grantHashes := map[string]string{}

	for _, municipality := range s.municipalities {
		grantHashes[municipality] = r.URL.Query().Get(municipality)
	}

	p := retrievePage{
		basePage: &basePage{
			SelfOrganizationName: s.selfOrganizationName,
			ActivePagePath:       PagePathRetrieve,
			PagePathHome:         PagePathHome,
			PagePathRetrieve:     PagePathRetrieve,
			PagePathAdd:          PagePathAdd,
		},
		Municipalities: s.municipalities,
		Parkeerrechten: []retrievePageParkeerrecht{},
		Form: retrievePageForm{
			Hashes: grantHashes,
		},
	}

	for _, municipality := range s.municipalities {
		grantHash := r.URL.Query().Get(municipality)

		if grantHash == "" {
			continue
		}

		municipalityParkeerrechten, err := fetchParkeerrechten(s.outwayAddress, grantHash)
		if err != nil {
			http.Error(w, fmt.Sprintf("failed to fetch parkeerrechten for %s: %s\n", municipality, err), http.StatusInternalServerError)
			return
		}

		for _, parkeerrecht := range municipalityParkeerrechten {
			p.Parkeerrechten = append(p.Parkeerrechten, retrievePageParkeerrecht{
				Kenteken:                parkeerrecht.Kenteken,
				JaartalGeldigheidrecht:  parkeerrecht.JaartalGeldigheidrecht,
				GemeenteGeldigheidrecht: parkeerrecht.GemeenteGeldigheidrecht,
			})
		}
	}

	t, err := template.ParseFS(
		tplFolder,
		"templates/base.html",
		"templates/retrieve.html",
	)
	if err != nil {
		fmt.Printf("failed to parse retrieve template: %s", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	err = t.ExecuteTemplate(w, "base.html", p)
	if err != nil {
		fmt.Printf("failed to render template: %s", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}

type retrievePage struct {
	*basePage

	Municipalities []string
	Parkeerrechten []retrievePageParkeerrecht
	GrantHashes    map[string]string
	Form           retrievePageForm
}

type retrievePageForm struct {
	Hashes map[string]string
}

type retrievePageParkeerrecht struct {
	Kenteken                string
	JaartalGeldigheidrecht  int
	GemeenteGeldigheidrecht string
}

func fetchParkeerrechten(outwayBaseURL, grantHash string) (apiResponseParkeerrechten, error) {
	if grantHash == "" {
		return nil, errors.New("no grant hash provided")
	}

	requestURL := fmt.Sprintf("%s/parkeerrechten", outwayBaseURL)
	log.Printf("parkeerrechten request url: %s", requestURL)

	req, err := http.NewRequest(http.MethodGet, requestURL, nil)
	if err != nil {
		return nil, fmt.Errorf("client: could not create request to outway %s: %s", outwayBaseURL, err)
	}

	log.Printf("grant hash header value: %s", grantHash)

	req.Header.Add("Fsc-Grant-Hash", grantHash)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("client: %d, error making http request to outway %s: %s", res.StatusCode, outwayBaseURL, err)
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("client: %d, could not read response body: %s\n", res.StatusCode, err)
	}

	log.Printf("parkeerrechten response body: %s", body)

	var parkeerrechten apiResponseParkeerrechten
	err = json.Unmarshal(body, &parkeerrechten)
	if err != nil {
		return nil, fmt.Errorf("%d, error unmarshaling json from body: %s err: %s", res.StatusCode, body, err)
	}

	return parkeerrechten, nil
}

type apiResponseParkeerrechten []apiResponseParkeerrecht

type apiResponseParkeerrecht struct {
	Kenteken                string `json:"kenteken"`
	JaartalGeldigheidrecht  int    `json:"jaartalGeldigheidrecht"`
	GemeenteGeldigheidrecht string `json:"gemeenteGeldigheidrecht"`
}
