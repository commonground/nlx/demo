module go.nlx.io/demo

go 1.20

require (
	github.com/caarlos0/env v3.5.0+incompatible // indirect
	github.com/go-chi/chi/v5 v5.0.8 // indirect
)
