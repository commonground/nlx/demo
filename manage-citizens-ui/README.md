# Manage Citizens UI

This app is used by the demo municipalities (eg. Stijns) to manage citizens
from the `basisregister-fictieve-kentekens` service via NLX.

## Development

Make sure you first run the `basisregister-fictieve-personen` service in a terminal.
See the [README of basisregister-fictieve-personen](../basisregister-fictieve-personen/README.md) on how to do this.

The application expects the video being served from an Outway.
Specify the Outway address including the organization and service via the variable `OUTWAY_PROXY_URL`.

Example: `BASISREGISTER_FICTIEVE_KENTEKENS_API_BASE_URL=http://my-outway/<organization>/<service>/`

The organization name can be set using `ORGANIZATION_NAME`.
The organization logo can be set using `ORGANIZATION_LOGO`.

Run the development server of this app with:

```shell
npm install
npm start
```
