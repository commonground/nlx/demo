// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

const fetchJson = async (url, additionalHeaders) => {
  // 1. perform the request
  const response = await fetch(url, {
    headers: additionalHeaders,
  })

  // 2. verify the response status code
  // 'ok' means the status code is in the range (200-299 inclusive)
  // we throw an error with extra context for debugging purpose:
  // * the url we requested
  // * the status code of the response (integer)
  // * the statusText (string which defaults to "")
  if (!response.ok) {
    const responseText = await response.text()
    throw new Error(
      `request to ${url} was unsuccessful. status code: ${response.status}. response text: ${responseText}`,
    )
  }

  // we clone the response so we can read from it again
  // when the response is not JSON
  // via https://stackoverflow.com/a/40497935
  const responseClone = response.clone()

  try {
    return await response.json()
  } catch {
    // 3. we expected JSON but it seems like the response did not contain valid JSON.
    // we've cloned the response object, so we can read the output as text and use for debugging
    const body = await responseClone.text()
    throw new Error(
      `received invalid json from request to ${url}. status code: ${response.status}. status text: ${response.statusText}. response: ${body}`,
    )
  }
}

export default fetchJson
