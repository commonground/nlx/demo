// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import { ThemeProvider } from 'styled-components'
import Head from 'next/head'
import { GlobalStyles, defaultTheme } from '@commonground/design-system'
import '@fontsource/source-sans-pro/latin.css'
import getConfig from 'next/config'
const { publicRuntimeConfig } = getConfig()

function App({ Component, pageProps }) {
  const organizationName = publicRuntimeConfig.ORGANIZATION_NAME

  return (
    <ThemeProvider theme={defaultTheme}>
      <Head>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <title>{organizationName} - NLX demo</title>
      </Head>
      <GlobalStyles />
      <Component {...pageProps} />
    </ThemeProvider>
  )
}

export default App
