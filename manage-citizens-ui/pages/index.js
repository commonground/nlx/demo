// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

import getConfig from 'next/config'
import styled from 'styled-components'
import { Alert } from '@commonground/design-system'
import fetchJsonUtility from '../fetch-json-utility'
const { serverRuntimeConfig } = getConfig()

const Container = styled.div`
  margin: 10vh auto 0 auto;
  width: 650px;
  max-width: 100%;
`

const Table = styled.table`
  border-collapse: collapse;
  width: 100%;
`

const Td = styled.td`
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
`

const Home = ({ inhabitants, error, organizationName }) => {
  return (
    <Container>
      <h1>Manage citizens using NLX</h1>
      <p>
        Example application for municipalities to manage their inhabitants.
        Created to be used at the &apos;Hackathon Samenwerkende APIs&apos;.
      </p>

      <h2>The request flow</h2>
      <p>
        NextJS UI (this interface) → NextJS API → Outway ({organizationName}) →
        Inway ({organizationName}) → Basisregister Fictieve Personen (API)
      </p>
      <p>Make sure you have access to the service that is serving the API.</p>

      <h2>Records from basisregister-fictieve-personen</h2>

      {error && (
        <Alert variant="error" title="Unexpected error">
          {error}
        </Alert>
      )}

      <Table>
        <thead>
          <tr>
            <th style={{ textAlign: 'left' }}>Name</th>
            <th style={{ textAlign: 'left' }}>BSN</th>
            <th style={{ textAlign: 'left' }}>Municipality</th>
          </tr>
        </thead>
        <tbody>
          {inhabitants.map((inhabitant, i) => (
            <tr key={i}>
              <Td>
                {inhabitant.naam.voornamen} {inhabitant.naam.geslachtsnaam}
              </Td>
              <Td>{inhabitant.burgerservicenummer}</Td>
              <Td>{inhabitant.postadres.woonplaats}</Td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Container>
  )
}

export async function getServerSideProps() {
  const baseUrl =
    serverRuntimeConfig.BASISREGISTER_FICTIEVE_PERSONEN_API_BASE_URL
  const organizationName = serverRuntimeConfig.ORGANIZATION_NAME

  try {
    const url = `${baseUrl}/natuurlijke_personen`
    const data = await fetchJsonUtility(url)
    return {
      props: {
        inhabitants: data.resultaten,
        organizationName: organizationName,
      },
    }
  } catch (error) {
    return {
      props: {
        inhabitants: [],
        error: error.message,
        organizationName: organizationName,
      },
    }
  }
}

export default Home
