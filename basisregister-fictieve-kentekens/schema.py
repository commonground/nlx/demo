from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin
from marshmallow import Schema, fields


spec = APISpec(
    title='Basisregister Fictieve Kentekens (BFK)',
    version='1.0.0',
    openapi_version='3.0.2',
    plugins=[FlaskPlugin(), MarshmallowPlugin()],
)


class VoertuigSchema(Schema):
    kenteken = fields.String(required=True)
    burgerservicenummer = fields.String(required=True)
    datum_tenaamstelling = fields.String(required=True)
    eerste_kleur = fields.String(required=True)
    europese_voertuigcategorie = fields.String(required=True)
    handelsbenaming = fields.String(required=True)
    merk = fields.String(required=True)
    voertuigsoort = fields.String(required=True)


class VoertuigLijstSchema(Schema):
    aantal = fields.Integer(required=True)
    resultaten = fields.Nested('VoertuigSchema', required=True, many=True)


spec.components.schema('Voertuig', schema=VoertuigSchema)
spec.components.schema('VoertuigLijst', schema=VoertuigLijstSchema)
