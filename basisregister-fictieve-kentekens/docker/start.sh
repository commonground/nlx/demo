#!/usr/bin/env sh

PORT="${PORT:-8000}"

USE_TLS="${USE_TLS:-0}"
TLS_CERT="${TLS_CERT}"
TLS_KEY="${TLS_KEY}"

if [ "${USE_TLS}" == "1" ]; then
  PARAMS="--https :${PORT},${TLS_CERT},${TLS_KEY},HIGH"
else
  PARAMS="--http :${PORT}"
fi

echo "Listening on :${PORT}"
uwsgi ${PARAMS} --manage-script-name --mount /=server:app
