# Basisregister Fictieve Kentekens

## Setup your development environment

### Virtualenv and packages

Make sure you created a virtual environment, using:

```
python3 -m venv .venv
```

Then activate the virtual environment:

```
source .venv/bin/activate
```

Then install the requirements based on the environment:

```
pip install -r requirements.txt
```

### Development server

You can run the project using:

```shell
python3 server.py
```

## Docker

There is a Docker Compose file provided for development purposes.
Start the API using the following command:

```
docker-compose up
```
