// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
let servicesList
let serviceOrganizations

try {
  servicesList = JSON.parse(process.env.SERVICES_LIST)
  serviceOrganizations = servicesList.map((service) => service.organization)
} catch (e) {
  console.error('Error decoding SERVICES_LIST: ', process.env.SERVICES_LIST)
  console.error(e)
}

module.exports = {
  serverRuntimeConfig: {
    SERVICES_LIST: servicesList,
    KENTEKEN_API_BASE_URL: process.env.KENTEKEN_API_BASE_URL,
    PERSONEN_API_BASE_URL: process.env.PERSONEN_API_BASE_URL,
  },
  publicRuntimeConfig: {
    ORGANIZATION_NAME: process.env.ORGANIZATION_NAME,
    ORGANIZATION_LOGO: process.env.ORGANIZATION_LOGO,
    SERVICE_ORGANIZATIONS: serviceOrganizations,
  },
  reactStrictMode: true,
  compiler: {
    styledComponents: true,
  },
}
