// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import { useEffect, useState } from 'react'
import {
  Alert,
  Button,
  Fieldset,
  Legend,
  Label,
  SelectComponent,
} from '@commonground/design-system'
import Input from '@commonground/design-system/dist/components/Form/TextInput/Input'
import getConfig from 'next/config'
import fetchJson from '../data/fetch-json-utility'

const { publicRuntimeConfig } = getConfig()

const municipalityOptions = publicRuntimeConfig.SERVICE_ORGANIZATIONS.map(
  (organization) => ({ label: organization, value: organization }),
)

const currentYear = new Date().getFullYear()

const Add = ({ bearerToken, orderReference, orderDelegator }) => {
  const [license, setLicense] = useState('')
  const [municipality, setMunicipality] = useState('')
  const [year, setYear] = useState(currentYear)
  const [licenseDetails, setLicenseDetails] = useState()
  const [isGemeenteValid, setIsGemeenteValid] = useState()
  const [error, setError] = useState()
  const [isSuccessfullyAdded, setIsSuccessfullyAdded] = useState(false)

  useEffect(async () => {
    await fetchLicenseDetails(license)
  }, [license])

  useEffect(async () => {
    const isGemeenteValid =
      licenseDetails && licenseDetails.person.residence === municipality
    setIsGemeenteValid(isGemeenteValid)
  }, [licenseDetails, municipality])

  const fetchLicenseDetails = async (license) => {
    setLicenseDetails(null)
    setError(null)

    if (!license || license.length < 1) {
      return
    }

    try {
      const headers = {}

      if (bearerToken && bearerToken.length > 0) {
        headers['x-nlx-authorization'] = `Bearer ${bearerToken}`
      }

      if (orderDelegator && orderDelegator.length > 0) {
        headers['x-nlx-request-delegator'] = orderDelegator
      }

      if (orderReference && orderReference.length > 0) {
        headers['x-nlx-request-order-reference'] = orderReference
      }

      const data = await fetchJson(
        `/api/getDataByKenteken?k=${license.toUpperCase()}`,
        {
          headers: headers,
        },
      )
      setLicenseDetails(data)
      setError(null)
    } catch (error) {
      setLicenseDetails(null)
      setError(error.message)
    }
  }

  const submitForm = async (event) => {
    event.preventDefault()

    setIsSuccessfullyAdded(false)

    try {
      const headers = {}

      if (bearerToken && bearerToken.length > 0) {
        headers['x-nlx-authorization'] = `Bearer ${bearerToken}`
      }

      if (orderDelegator && orderDelegator.length > 0) {
        headers['x-nlx-request-delegator'] = orderDelegator
      }

      if (orderReference && orderReference.length > 0) {
        headers['x-nlx-request-order-reference'] = orderReference
      }

      const response = await fetch('/api/postParkeerrechten', {
        headers: headers,
        method: 'POST',
        body: JSON.stringify({
          gemeenteGeldigheidrecht: municipality,
          jaartalGeldigheidrecht: year,
          kenteken: license,
        }),
      })

      if (response.status === 500) {
        const data = await response.json()
        throw new Error(data.error)
      }

      if (!response.ok) {
        throw new Error('unexpected error, failed to add parkeerrecht')
      }

      setError(null)
      setIsSuccessfullyAdded(true)
    } catch (error) {
      setLicenseDetails(null)
      setError(error.message)
    }
  }

  return (
    <>
      <h2>Parkeerrecht toevoegen</h2>

      <p>
        Om een parkeerrecht te kunnen toevoegen, moet er toegang zijn tot zowel
        de gemeente(s) als de rijksdienst.
      </p>

      {isSuccessfullyAdded && (
        <Alert variant="success" title="Parkeerrecht toegevoegd">
          Het parkeerrecht werd toegevoegd.
        </Alert>
      )}

      {error && (
        <Alert variant="error" title="Unexpected error">
          {error}
        </Alert>
      )}

      <form onSubmit={submitForm}>
        <Fieldset>
          <Label htmlFor="municipality">Gemeente</Label>
          <small>Gemeente waarvoor het parkeerrecht geldig is</small>
          <SelectComponent
            inputId="municipality"
            name="municipality"
            placeholder="Selecteer gemeente"
            options={municipalityOptions}
            onChange={(item) => setMunicipality(item.value)}
          />

          <Label htmlFor="license">Kenteken</Label>
          <small>Bijvoorbeeld &apos;KN958B&apos;</small>
          <Input
            id="license"
            placeholder="Bv. KN958B"
            size="m"
            defaultValue={license}
            onChange={(e) => setLicense(e.target.value)}
          />

          <Label>Geldig voor</Label>
          <small>Geef het jaar op waarop het parkeerrecht geldig is.</small>
          <Input
            placeholder="2022"
            size="m"
            type="number"
            defaultValue={year}
            onChange={(e) => setYear(e.target.value)}
          />
        </Fieldset>

        {licenseDetails && (
          <Fieldset>
            <Legend>Details kenteken</Legend>
            <dl>
              <dt>Type auto</dt>
              <dd>{licenseDetails.vehicle.name}</dd>

              <dt>Op naam van</dt>
              <dd>{licenseDetails.person.name}</dd>

              <dt>Adres</dt>
              <dd>
                {licenseDetails.person.address} te{' '}
                {licenseDetails.person.residence}
              </dd>
            </dl>

            {!isGemeenteValid && (
              <Alert>
                Kan geen parkeerrecht toevoegen. Deze persoon komt uit een
                andere gemeente.
              </Alert>
            )}
          </Fieldset>
        )}

        <Button type="submit">Toevoegen</Button>
      </form>
    </>
  )
}

export default Add
