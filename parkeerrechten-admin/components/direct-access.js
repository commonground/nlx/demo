// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import styled from 'styled-components'
import { useState } from 'react'
import {
  Alert,
  Button,
  Fieldset,
  Label,
  Legend,
} from '@commonground/design-system'
import Input from '@commonground/design-system/dist/components/Form/TextInput/Input'
import fetchJsonUtility from '../data/fetch-json-utility'
import Add from './add'

const Table = styled.table`
  border-collapse: collapse;
  width: 100%;
`

const Th = styled.th`
  text-align: left;
`

const Td = styled.td`
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
`

const defaultBearerToken =
  '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'

const DirectAccess = () => {
  const [bearerToken, setBearerToken] = useState(defaultBearerToken)
  const [parkeerrechten, setParkeerrechten] = useState([])
  const [error, setError] = useState()

  const fetchData = async () => {
    setParkeerrechten([])
    setError(null)

    try {
      const data = await fetchJsonUtility('/api/getParkeerrechten', {
        headers: {
          'X-Nlx-Authorization': `Bearer ${bearerToken}`,
        },
      })
      setParkeerrechten(data)
      setError(null)
    } catch (error) {
      setParkeerrechten([])
      setError(error.message)
    }
  }

  return (
    <>
      {error && (
        <Alert variant="error" title="Unexpected error">
          {error}
        </Alert>
      )}

      <form>
        <p>
          Met deze demo-applicatie is het mogelijk om Parkeerrechten te beheren
          met behulp van NLX. We halen de Parkeerrechten van Vergunningsoftware
          BV op bij Gemeente Stijns en Gemeente Riemer.
        </p>

        <p>
          De parkeerrechten ophalen en toevoegen kan op twee verschillende
          manieren. Door zelf toegang te regelen of via Delegatie. De flow is
          steeds als volgt:
        </p>

        <Legend>Request flow</Legend>

        <p>Client (deze website) → Outway → Inway → Parkeerrechten (API)</p>

        <ol>
          <li>
            Client → Outway <br /> Outway draait binnen hetzelfde netwerk als de
            Client. Voor elke gemeente maken we een request naar onze Outway en
            geven telkens de bijhorende Grant Hash mee in de header{' '}
            <code>Fsc-Grant-Hash</code>.
          </li>
          <li>
            Outway → Inway <br />
          </li>
          <li>Inway → Parkeerrechten (API)</li>
        </ol>
        <p></p>

        <Fieldset>
          <Legend>Toegang op basis van Grants</Legend>
          <p>
            Als je over de juiste Grants in een geldig contract beschikt kunnen
            we een request maken naar de Parkeerrechten API. We geven de Grant
            mee aan onze Outway door gebruik te maken van de header{' '}
            <code>Fsc-Grant-Hash</code>.
          </p>

          <p>
            Hieronder kan je opgeven welke Grant Hashes je wil gebruiken voor
            het communiceren met de verschillende services.&nbsp;
            <a href="" target="_blank" rel="nofollow noreferrer">
              Beheer je contracten in Management
            </a>
            .
          </p>

          <Label>Grant Hash Kenteken service</Label>
          <Input
            placeholder="Grant Hash"
            size="m"
            defaultValue=""
            onChange={(e) => setBearerToken(e.target.value)}
          />

          <Label>Grant Hash Parkeerrechten service Gemeente Stijns</Label>
          <Input
            placeholder="Grant Hash"
            size="m"
            defaultValue=""
            onChange={(e) => setBearerToken(e.target.value)}
          />

          <Label>Grant Hash Parkeerrechten service Gemeente Riemer</Label>
          <Input
            placeholder="Grant Hash"
            size="m"
            defaultValue=""
            onChange={(e) => setBearerToken(e.target.value)}
          />
        </Fieldset>
      </form>

      <Table>
        <thead>
          <tr>
            <Th>#</Th>
            <Th>Kenteken</Th>
            <Th>Geldig in</Th>
            <Th>Gemeente</Th>
          </tr>
        </thead>

        <tbody>
          {parkeerrechten.length ? (
            parkeerrechten.map((parkeerrecht, i) => (
              <tr key={i}>
                <Td>{i + 1}</Td>
                <Td>{parkeerrecht.kenteken}</Td>
                <Td>{parkeerrecht.jaartalGeldigheidrecht}</Td>
                <Td>{parkeerrecht.gemeenteGeldigheidrecht}</Td>
              </tr>
            ))
          ) : (
            <tr>
              <Td colSpan="4">Geen parkeerrechten gevonden</Td>
            </tr>
          )}
        </tbody>
      </Table>

      <br />

      <Button type="button" onClick={fetchData}>
        Opnieuw ophalen
      </Button>

      <Add bearerToken={bearerToken} />

      <h2>Outway authorisatie (via OPA)</h2>
      <form action="">
        <Fieldset>
          <p>
            Om specifieker de toegang te kunnen regelen kan je authorizatie
            uitvoeren op de Outway. In dit voorbeeld maakt de Outway gebruik van
            Open Policy Agent (OPA). Hiermee kan een verfijning op de toegang op
            organisatieniveau ingezet worden. In deze applicatie hebben we een
            tweetal voorbeeld gebruikers toegevoegd die toegang hebben.
          </p>

          <Label>Bearer token</Label>
          <small>
            <a
              href="https://gitlab.com/commonground/nlx/nlx/-/blob/master/auth-opa/outway/data/users.json"
              target="_blank"
              rel="nofollow noreferrer"
            >
              Lijst van alle beschikbare tokens op GitLab
            </a>
          </small>
          <Input
            placeholder="Bearer token"
            size="m"
            defaultValue={defaultBearerToken}
            onChange={(e) => setBearerToken(e.target.value)}
          />
        </Fieldset>
      </form>
    </>
  )
}

export default DirectAccess
