# Parkeerrechten admin

This app is used by the demo organization 'Vergunningsoftware BV' to list and 
create parkeervergunningen for a municipiality.

## Development

Make sure you first run the dev server of `parkeerrechten-api` in a terminal.  
See the [README of parkeerrechten-api](../parkeerrechten-api/README.md) on how to do this.

Also run `basisregister-fictieve-kentekens` and `-personen` as described in their readme files:  
[kentekens readme](../basisregister-fictieve-kentekens/README.md) | [personen readme](../basisregister-fictieve-personen/README.md).

Next, open a second terminal and run de development server of this app:

```shell
npm install
npm start
```
