TODO om werkend te krijgen voor FSC
---

- Demo-app opnieuw bouwen met Golang SSR & Htmx
- "De data loopt via NLX." -> "De data loopt via FSC."? Of houden we NLX aan?
- Nakijken of OPA op Outway nog werkt lokaal en op Review
- Pagina's directe toegang en toegang via een opdracht kunnen worden samengevoegd.
  Voor beide heb je een grant hash van een geldig contract nodig.
- URL voor `management` van eigen organisatie toevoegen?
  Dan kan je makkelijk de juiste Grant Hash zoeken.

## Direct toegang

Voor zowel ophalen als wegschrijven parkeerrechten:

- Je moet per gemeente kunnen opgeven met welk contract je het request wil maken.
1. Lijst van content hashes
2. Input voor content hash

## Toegang via opdracht

