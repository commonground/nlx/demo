// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import getConfig from 'next/config'
import {
  extractApiAuthorizationHeader,
  extractApiOrderHeaders,
} from '../../data/fetch-json-utility'
const { serverRuntimeConfig } = getConfig()

export default async function handler(req, res) {
  const values = JSON.parse(req.body)

  const service =
    values.gemeenteGeldigheidrecht &&
    serverRuntimeConfig.SERVICES_LIST.find(
      (service) => service.organization === values.gemeenteGeldigheidrecht,
    )

  if (!service) {
    res.status(500).json({
      error: 'Kan geen parkeerrecht opslaan voor onbekende gemeente',
    })
    return
  }

  try {
    const response = await fetch(`${service.baseUrl}/parkeerrechten/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        ...extractApiOrderHeaders(req),
        ...extractApiAuthorizationHeader(req),
      },
      body: req.body,
    })

    const json = await response.json()
    res.status(response.status).json(json.results)
  } catch (error) {
    res.status(500).json({ error: error.message })
  }
}
