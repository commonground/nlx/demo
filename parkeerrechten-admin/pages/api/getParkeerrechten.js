// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import getConfig from 'next/config'
import fetchJson from '../../data/fetch-json-utility'

const { serverRuntimeConfig } = getConfig()
const baseUrls = serverRuntimeConfig.SERVICES_LIST.map(
  (service) => service.baseUrl,
)

export default async function handler(req, res) {
  try {
    const headers = {}

    if (req.headers['x-nlx-authorization']) {
      headers['x-nlx-authorization'] = req.headers['x-nlx-authorization']
    }

    if (req.headers['x-nlx-request-order-reference']) {
      headers['x-nlx-request-order-reference'] =
        req.headers['x-nlx-request-order-reference']
    }

    if (req.headers['x-nlx-request-delegator']) {
      headers['x-nlx-request-delegator'] =
        req.headers['x-nlx-request-delegator']
    }

    // eslint-disable-next-line no-console
    console.log('headers to send when retrieving parkeerrechten: ', headers)

    const results = await Promise.all(
      baseUrls.map((baseUrl) =>
        fetchJson(`${baseUrl}/parkeerrechten/`, {
          headers: headers,
        }),
      ),
    )

    const json = Array.prototype.concat(...results)

    res.status(200).json(json)
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log('failed to retrieve parkeerrechten: ', error)

    res.status(500).json({ error: error.message })
  }
}
