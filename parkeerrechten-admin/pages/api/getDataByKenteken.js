// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import getConfig from 'next/config'
import fetchJson, {
  extractApiAuthorizationHeader,
  extractApiOrderHeaders,
} from '../../data/fetch-json-utility'

const { serverRuntimeConfig } = getConfig()
const kentekenBaseUrl = serverRuntimeConfig.KENTEKEN_API_BASE_URL
const personenBaseUrl = serverRuntimeConfig.PERSONEN_API_BASE_URL

export default async function handler(req, res) {
  const kenteken = req.query.k

  if (!kenteken) {
    throw Error('Kenteken niet meegegeven')
  }

  let kentekenJSON
  const kentekenUrl = `${kentekenBaseUrl}/voertuig/${kenteken}`
  const additionalHeaders = {
    ...extractApiOrderHeaders(req),
    ...extractApiAuthorizationHeader(req),
  }

  try {
    kentekenJSON = await fetchJson(kentekenUrl, {
      headers: additionalHeaders,
    })
  } catch (error) {
    console.error('failed to retrieve kenteken: ', error.message)
    res
      .status(500)
      .json({ error: 'failed to retrieve kenteken: ' + error.message })
    return
  }

  if (!kentekenJSON.burgerservicenummer) {
    throw Error(`Kenteken (${kenteken}) bevat geen burgerservicenummer`)
  }

  let persoonJSON
  const persoonUrl = `${personenBaseUrl}/natuurlijke_personen/bsn/${kentekenJSON.burgerservicenummer}`

  try {
    persoonJSON = await fetchJson(persoonUrl, {
      headers: additionalHeaders,
    })
  } catch (error) {
    console.error('failed to retrieve persoon: ', error.message)
    res
      .status(500)
      .json({ error: 'failed to retrieve persoon: ' + error.message })
    return
  }

  const { voorletters, geslachtsnaam } = persoonJSON.persoon.aanschrijving
  const { straatnaam, huisnummer, woonplaats } =
    persoonJSON.persoon.verblijfadres

  res.status(200).json({
    vehicle: {
      licence: kentekenJSON.kenteken,
      name: kentekenJSON.handelsbenaming,
    },
    person: {
      name: `${voorletters} ${geslachtsnaam}`,
      address: `${straatnaam} ${huisnummer}`,
      residence: woonplaats,
    },
  })
}
