// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import DirectAccess from '../../components/direct-access'

const Direct = ({ children }) => {
  return (
    <>
      <DirectAccess />

      {children}
    </>
  )
}

export default Direct
