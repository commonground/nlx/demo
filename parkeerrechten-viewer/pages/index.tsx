// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import Home from 'ui/pages/Home'

export default function Index() {
  return <Home />
}

export const getServerSideProps = () => ({ props: {} })
