// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
module.exports = {
  serverRuntimeConfig: {
    PARKEERRECHTEN_API_BASE_URL: process.env.PARKEERRECHTEN_API_BASE_URL,
  },
  publicRuntimeConfig: {
    ORGANIZATION_NAME: process.env.ORGANIZATION_NAME,
    ORGANIZATION_LOGO: process.env.ORGANIZATION_LOGO,
    ORGANIZATION_COLOR_PRIMARY: process.env.ORGANIZATION_COLOR_PRIMARY,
  },
}
