// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import { useRef, useState, useEffect } from 'react'
import Head from 'next/head'
import getConfig from 'next/config'
import styled from 'styled-components'
import Header from 'ui/components/Header'
import Content from 'ui/components/Content'
import Loading from 'ui/components/Loading'
import Parkeerrechten from 'ui/components/Parkeerrechten'

const Error = styled.p`
  color: ${(p) => p.theme.tokens.colorError};
`

const { publicRuntimeConfig } = getConfig()

const fetchData = async (
  refFetchController,
  setParkeerrechten,
  setLoading,
  setError,
) => {
  if (!refFetchController) {
    refFetchController.current.abort()
    refFetchController.current = new AbortController()
  }

  const response = await fetch('/api/getParkeerrechten', {
    signal: refFetchController?.current.signal,
  })

  const json = await response.json()

  setLoading(false)

  if (response.ok && response.status === 200) {
    setParkeerrechten(json)
    setError(false)
  } else {
    setError(true)
    console.error(json.error)
  }
}

export default function Home() {
  const refFetchController = useRef(
    typeof window === 'undefined' ? null : new AbortController(),
  )
  const [parkeerrechten, setParkeerrechten] = useState([])
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(false)

  useEffect(() => {
    fetchData(refFetchController, setParkeerrechten, setLoading, setError)
    return () => refFetchController?.current?.abort()
  }, [])

  const PageContents = () => {
    if (loading) {
      return <Loading />
    }
    if (error) {
      return <Error>Fout bij het ophalen van parkeerrechten</Error>
    }
    return <Parkeerrechten parkeerrechten={parkeerrechten} />
  }

  return (
    <>
      <Head>
        <title>{publicRuntimeConfig.ORGANIZATION_NAME} - NLX demo</title>
      </Head>
      <Header />
      <Content>
        <PageContents />
      </Content>
    </>
  )
}
