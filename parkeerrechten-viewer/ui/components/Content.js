// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'

const Content = styled.main`
  margin: ${(p) => p.theme.tokens.spacing05};
`

export default Content
