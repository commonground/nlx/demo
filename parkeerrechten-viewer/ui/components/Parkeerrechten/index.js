// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'

const Table = styled.table`
  width: 500px;
  max-width: 100%;
`

const Th = styled.th`
  padding: ${(p) => p.theme.tokens.spacing03};
  border-bottom: 1px solid ${(p) => p.theme.tokens.colorPaletteGray300};
  text-align: left;
`

const Td = styled.td`
  padding: ${(p) => p.theme.tokens.spacing03};
`

const Parkeerrechten = ({ parkeerrechten }) => (
  <Table>
    <thead>
      <tr>
        <Th>Kenteken</Th>
        <Th>Geldig in</Th>
      </tr>
    </thead>

    <tbody>
      {parkeerrechten.length ? (
        parkeerrechten.map((parkeerrecht, i) => (
          <tr key={i}>
            <Td>{parkeerrecht.kenteken}</Td>
            <Td>{parkeerrecht.jaartalGeldigheidrecht}</Td>
          </tr>
        ))
      ) : (
        <tr>
          <Td colSpan="3">Geen parkeerrechten gevonden</Td>
        </tr>
      )}
    </tbody>
  </Table>
)

export default Parkeerrechten
