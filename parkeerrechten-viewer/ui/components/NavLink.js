// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import { useRouter } from 'next/router'

const NavLink = ({ to, className, children, Icon, ...props }) => {
  const { basePath, pathname } = useRouter()
  const href = basePath + to
  const finalClassName = pathname === to ? `${className} active` : className

  return (
    <a href={href} className={finalClassName} {...props}>
      {children}
    </a>
  )
}

export default NavLink
