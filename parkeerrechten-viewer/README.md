# Parkeerrechten viewer

This app is used by the demo municipalities (eg. Stijns) to display the contents of their own parkeerrechten service.

## Development

Make sure you first run the dev server of `parkeerrechten-api` in a terminal. How to do this in it's readme.

Then open a second terminal and run de development server of this app with:

```shell
npm install
npm start
```

### A note about environments

To speed up the build process for demo we create one image of this app, which is used to create separate containers for different organizations.  
We use environment variables to tell the difference, which are converted to runtime variables in `next.config.js`.

In order for these variables to work, we need to instruct NextJS to create a server-rendered page. That's why we need to add the following line to pages that use these runtime variables.

```js
export const getServerSideProps = () => ({ props: {} })
```

> If we didn't do this, a static html page would be rendered by NextJS in the `build` step of Dockerfile and runtime variables can't be read.
