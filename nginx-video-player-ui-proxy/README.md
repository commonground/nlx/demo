# Nginx Video Player Proxy

This proxy is used by the demo municipalities (eg. Stijns) to demonstrate that NLX supports using video-player-ui.
The proxy is required, since we don't recommend users to expose the Outway directly to the internet.

It is created to be used with the `video-player-ui`.
