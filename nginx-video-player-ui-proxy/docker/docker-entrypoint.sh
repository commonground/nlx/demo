#!/usr/bin/env sh
set -eu

envsubst '${OUTWAY_PROXY_URL}' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf

exec "$@"
