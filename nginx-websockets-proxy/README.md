# Nginx Websockets Proxy

This proxy is used by the demo municipalities (eg. Stijns) to demonstrate that NLX supports using websockets.
The proxy is required, since we don't recommend users to expose the Outway directly to the internet.

It is created to be used with the `websockets-chat-ui`.

## Development

The Outway to proxy can be set using `OUTWAY_SERVICE_BASE_URL`.

Run the development server of this app with:

```shell
docker-compose up
```
