// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import Head from 'next/head'
import getConfig from 'next/config'
import { useEffect, useState } from 'react'
import { Container } from './index.styles'

const { publicRuntimeConfig } = getConfig()

export default function Home() {
  const [websocketsProxyBaseUrl, setWebsocketsProxyBaseUrl] = useState(null)
  const [chats, setChats] = useState([])
  const [socket, setSocket] = useState(null)

  useEffect(() => {
    const load = async () => {
      const response = await fetch(`/api/getWebsocketsProxyBaseUrl`)
      const responseText = await response.text()
      setWebsocketsProxyBaseUrl(responseText)
    }

    load()
  }, [])

  useEffect(() => {
    if (!websocketsProxyBaseUrl || websocketsProxyBaseUrl.length < 1) {
      return
    }

    const ws = new WebSocket(`${websocketsProxyBaseUrl}/`)
    setSocket(ws)

    return () => {
      ws.close()
    }
  }, [websocketsProxyBaseUrl])

  useEffect(() => {
    if (socket === null) {
      return
    }

    socket.onopen = function () {
      addToChatLog(`[open] Connection established`)
    }

    socket.onmessage = function (event) {
      addToChatLog(`[server] ${event.data}`)
    }

    socket.onclose = function (event) {
      if (event.wasClean) {
        addToChatLog(
          `[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`,
        )
      } else {
        // e.g. server process killed or network down
        // event.code is usually 1006 in this case
        addToChatLog('[close] Connection died')
      }
    }

    socket.onerror = function (error) {
      addToChatLog(`[error] ${JSON.stringify(error)}`)
    }
  }, [socket, chats])

  function addToChatLog(message) {
    const newChats = Array.from(chats)
    newChats.push(message)
    setChats(newChats)
  }

  const onFormSubmit = (event) => {
    event.preventDefault()

    const message = event.target.message.value
    addToChatLog(`[client] ${message}`)
    socket.send(message)
  }

  return (
    <>
      <Head>
        <title>{publicRuntimeConfig.ORGANIZATION_NAME} - NLX demo</title>
      </Head>

      <Container>
        <h1>WebSocket example using NLX</h1>
        <p>
          Example application to demonstrate that it is possible to connect to
          Web Sockets using NLX. The chat below is being served by an API that
          is exposed on the NLX network.
        </p>

        <h2>The request flow</h2>
        <p>
          NextJS UI (chat log) → Nginx Web Socket proxy → Outway → Inway →
          Service (API)
        </p>
        <p>
          <code>{websocketsProxyBaseUrl}</code> is being used to serve the Web
          Socket.
        </p>
        <p>Make sure you have access to that service.</p>

        <h2>Send message</h2>

        <form method="post" onSubmit={onFormSubmit}>
          <label>
            <strong>Message</strong> <br />
            <input
              type="text"
              autoComplete="off"
              name="message"
              placeholder="Your message here..."
            />
          </label>

          <button type="submit">Send</button>
        </form>

        <h2>Chat log</h2>
        <div>
          {chats.map((chat, i) => (
            <p key={i}>
              <code>{chat}</code>
            </p>
          ))}
        </div>
      </Container>
    </>
  )
}
