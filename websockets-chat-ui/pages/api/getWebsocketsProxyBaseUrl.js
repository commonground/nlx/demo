// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import getConfig from 'next/config'

const { serverRuntimeConfig } = getConfig()
const websocketsProxyBaseUrl = serverRuntimeConfig.WEBSOCKETS_PROXY_BASE_URL

export default async function handler(req, res) {
  res.status(200).send(websocketsProxyBaseUrl)
}
