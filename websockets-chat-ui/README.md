# Web Sockets Chat UI

This app is used by the demo municipalities (eg. Stijns) to demonstrate using Web Sockets via NLX.

## Development

The organization name can be set using `ORGANIZATION_NAME`.

The Web Sockets proxy base URL can be set using `WEBSOCKETS_PROXY_BASE_URL`.

Run the development server of this app with:

```shell
npm install
npm start
```
