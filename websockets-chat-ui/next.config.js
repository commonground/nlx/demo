// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
module.exports = {
  serverRuntimeConfig: {
    WEBSOCKETS_PROXY_BASE_URL: process.env.WEBSOCKETS_PROXY_BASE_URL,
  },
  publicRuntimeConfig: {
    ORGANIZATION_NAME: process.env.ORGANIZATION_NAME,
  },
}
