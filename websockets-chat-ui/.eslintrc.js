// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
module.exports = {
  plugins: ['react'],
  extends: [
    'plugin:@next/next/recommended',
    'plugin:react/recommended',
    '@commonground/eslint-config',
  ],
  settings: {
    react: {
      version: 'detect',
    },
  },
  rules: {
    'react/react-in-jsx-scope': 'off', // since react@17
    'react/prop-types': 'off', // this is just a demo
  },
}
