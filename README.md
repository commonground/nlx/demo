# NLX - Demo
This repository contains the code that is used to showcase the functionality of NLX. It contains the following components:

- **basisregister-fictieve-personen (BFP)** an example service that provides a personal records database
- **demo-service** an example service that tells the current time and echos back NLX-specific headers
- **basisregister-fictieve-kentekens (BFK)** an example service that provides a registry of vehicles
- **parkeerrechten-api** an example service that provides a registry of parkeerrechten
- **parkeerrechten-admin** an example UI that enables managing parkeerrechten
- **video-player-ui** an example UI used to showcase that NLX supports video streaming

## Questions and contributions
Read more on how to ask questions, file bugs and contribute code and documentation in [`CONTRIBUTING.md`](CONTRIBUTING.md).

## Licence
Copyright © VNG Realisatie 2017
[Licensed under the EUPL](LICENCE.md)
