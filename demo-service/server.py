from datetime import datetime

from flask import Flask, jsonify
from flask_restful import Resource, Api, reqparse


app = Flask(__name__)
api = Api(app)


class Echo(Resource):
    @staticmethod
    def get():
        parser = reqparse.RequestParser()
        parser.add_argument('X-NLX-Request-Organization', location='headers')
        parser.add_argument('X-NLX-Request-Logrecord-Id', location='headers')
        args = parser.parse_args()

        return jsonify({
            'message': 'Hi there, greetings from the nlx-demo API!',
            'local_time': datetime.now().isoformat(),
            'nlx_request_organization': args['X-NLX-Request-Organization'],
            'nlx_request_logrecord_id': args['X-NLX-Request-Logrecord-Id']
        })


@app.after_request
def apply_security_headers(response):
    response.headers['X-Frame-Options'] = 'DENY'
    response.headers['X-Content-Type-Options'] = 'nosniff'
    response.headers['X-XSS-Protection'] = '1; mode=block'
    response.headers['Referrer-Policy'] = 'same-origin'
    response.headers['Content-Security-Policy'] = "default-src 'self'; style-src 'self' 'unsafe-inline'"
    response.headers['Cache-Control'] = 'max-age=0, no-cache, no-store, must-revalidate'
    response.headers['Pragma'] = 'no-cache'
    return response


api.add_resource(Echo, '/')

if __name__ == '__main__':
    app.run(debug=True)  # This debug mode is not executed when in uwsgi mode
