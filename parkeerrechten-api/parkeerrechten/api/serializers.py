import re

from rest_framework import serializers

from parkeerrechten.api.models import Parkeerrecht


class ParkeerrechtSerializer(serializers.HyperlinkedModelSerializer):
    kenteken = serializers.CharField()

    class Meta:
        model = Parkeerrecht
        fields = [
            'kenteken',
            'jaartalGeldigheidrecht',
            'gemeenteGeldigheidrecht'
        ]

    def create(self, validated_data):
        kenteken_original = validated_data.pop('kenteken')

        kenteken = extract_alphanumeric(kenteken_original).upper()

        if len(kenteken) > 6:
            raise serializers.ValidationError('kenteken must not exceed 6 characters')

        return Parkeerrecht.objects.create(**validated_data, kenteken=kenteken)


def extract_alphanumeric(value):
    return re.sub(r'\W+', '', value)
