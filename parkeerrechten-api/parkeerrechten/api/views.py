from parkeerrechten.api.models import Parkeerrecht
from parkeerrechten.api.serializers import ParkeerrechtSerializer
from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet


class ParkeerrechtViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, GenericViewSet):
    queryset = Parkeerrecht.objects.all()
    serializer_class = ParkeerrechtSerializer
