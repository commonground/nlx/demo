from django.urls import include
from django.urls import path
from rest_framework.schemas import get_schema_view

from parkeerrechten.api.router import router

schema_view = get_schema_view(title="Parkeerrechten API")

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('schema', schema_view),
]
