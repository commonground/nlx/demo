FROM python:3.11.4-alpine as build

RUN apk add --no-cache build-base postgresql-dev python3

COPY requirements /requirements

RUN pip install -r /requirements/production.txt

FROM python:3.11.4-alpine

RUN apk add --no-cache libpq

COPY --from=build /usr/local/lib/python3.11 /usr/local/lib/python3.11
COPY --from=build /usr/local/bin/ /usr/local/bin/

WORKDIR /api

COPY manage.py /api/
COPY parkeerrechten /api/parkeerrechten

RUN adduser -D -u 1001 api_user && \
  chown -R api_user:api_user /api

USER api_user

ENV DJANGO_SETTINGS_MODULE=parkeerrechten.settings.production

RUN python ./manage.py collectstatic --noinput

RUN python -m compileall /api

CMD [ \
    "gunicorn", "parkeerrechten.asgi:application", \
    "--user", "api_user", \
    "--group", "api_user", \
    "--bind", "0.0.0.0:8000", \
    "--worker-class", "uvicorn.workers.UvicornWorker" \
]
